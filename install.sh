#!/bin/sh

cd `dirname $0`
base_dir=`pwd`

if [ -d ~/.vim ];then
        rm -rfv ~/.vim
fi
if [ -f ~/.vimrc ];then
        rm -rfv ~/.vimrc
fi
ln -sfv $base_dir/vimrc ~/.vimrc
ln -sfv $base_dir/vim ~/.vim
today=`date +%Y%m%d`
#for i in $HOME/.vim $HOME/.vimrc $HOME/.gvimrc; do [ -e $i ] && 
if [ ! -e $base_dir/.vim/vundle ];then 
	echo "Installing Vundle";
	git clone https://github.com/gmarik/vundle.git $base_dir/vim/bundle/vundle	
fi



